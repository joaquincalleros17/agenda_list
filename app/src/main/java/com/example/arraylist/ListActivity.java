package com.example.arraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.view.Menu;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    TableLayout tblLista;
    ArrayList<Contacto> contactoList;
    ArrayList<Contacto> filterList;
    public static ArrayList<Contacto> contactos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        tblLista = (TableLayout) findViewById(R.id.tbLista);
        Bundle bundleObjet = getIntent().getExtras();
        contactoList = (ArrayList<Contacto>)bundleObjet.getSerializable("contacto");
        filterList = contactoList;
        Log.d("Cargar contactos", "contactoList length "+ contactoList.size());
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        cargarContactos();
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    private void removeElement() {
        tblLista.removeAllViews();

        TableRow headRow = new TableRow(ListActivity.this);
        TextView nombre = new TextView(ListActivity.this);
        nombre.setText(R.string.nombre);
        nombre.setTextSize(TypedValue.COMPLEX_UNIT_PT,8);
        TextView accion = new TextView(ListActivity.this);

        accion.setText(R.string.accion);
        accion.setTextSize(TypedValue.COMPLEX_UNIT_PT,8);
        headRow.addView(nombre);
        headRow.addView(accion);
        tblLista.addView(headRow);

        cargarContactos();
    }
    private void buscar(String nombre){
        ArrayList<Contacto> lista = new ArrayList<>();
        for(int i = 0; i < filterList.size(); i++){
            if(filterList.get(i).getNombre().contains(nombre)) {
                lista.add(filterList.get(i));
            }
        }
        contactoList = lista;
        tblLista.removeAllViews();
        cargarContactos();
    }
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        MenuItem menuItem = menu.findItem(R.id.txtBuscar);
        SearchView sv = (SearchView) menuItem.getActionView();
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                buscar(newText);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }





    public void cargarContactos(){
        for(int x=0 ; x<contactoList.size();x++){
            final Contacto c = new Contacto(contactoList.get(x));
            TableRow nRow = new TableRow(ListActivity.this);

            TextView nText = new TextView(ListActivity.this);
            nText.setText(c.getNombre());
            nText.setTextSize(TypedValue.COMPLEX_UNIT_DIP,9);
            nText.setTextColor((c.isFavorito())? Color.BLUE:Color.BLACK);
            nRow.addView(nText);
            Button nbutton = new Button(ListActivity.this);
            nbutton.setText(R.string.accver);
            nbutton.setTextSize(TypedValue.COMPLEX_UNIT_DIP,9);
            nbutton.setTextColor(Color.BLACK);
            Button btnBorrar = new Button (ListActivity.this);
            btnBorrar.setText("Borrar");
            btnBorrar.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            btnBorrar.setTextColor(Color.BLACK);

            nbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto",c);
                    oBundle.putInt("index",Integer.valueOf(v.getTag(R.string.contacto).toString()));
                    i.putExtras(oBundle);
                    setResult(RESULT_OK,i);
                    finish();
                }
            });
            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int y = 0; y < filterList.size(); y++){
                        if(filterList.get(y).getId() == (int) c.getId()){
                            filterList.remove(filterList.get(y));
                            contactos = filterList;
                        }
                    }
                    removeElement();
                    /* Se quitaron los elementos de la lista*//**/
                    Toast.makeText(ListActivity.this, "Se ha quitado de la lista", Toast.LENGTH_SHORT).show();
                }
            });

            nbutton.setTag(R.string.contacto,c);
            nbutton.setTag(R.string.contacto,x);
            nRow.addView(nbutton);
            btnBorrar.setTag("Borrar");
            btnBorrar.setTag("Elemento");
            nRow.addView(btnBorrar);
            tblLista.addView(nRow);

            /*no tnemos el contacto*/Log.d("Cargar contactos", "agregar view de contacto " + c.getNombre());
        }
    }
}
