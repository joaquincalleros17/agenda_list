package com.example.arraylist;

import java.io.Serializable;

public class Contacto implements Serializable {
    private int id;
    private String nombre;
    private String tel1;
    private String tel2;
    private String domicilio;
    private boolean favorito;
    private String notas;

    public Contacto(int id, String nombre, String tel1, String tel2, String domicilio, boolean favorito, String notas) {
        this.id = id;
        this.nombre = nombre;
        this.tel1 = tel1;
        this.tel2 = tel2;
        this.domicilio = domicilio;
        this.favorito = favorito;
        this.notas = notas;
    }

    public Contacto(Contacto c) {
        this.id = c.id;
        this.nombre = c.nombre;
        this.tel1 = c.tel1;
        this.tel2 = c.tel2;
        this.domicilio = c.domicilio;
        this.favorito = c.favorito;
        this.notas = c.notas;
    }

    public Contacto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }
}
