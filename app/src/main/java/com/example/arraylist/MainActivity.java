package com.example.arraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final ArrayList<Contacto> contacto = new ArrayList<>();
    EditText edtNombre;
    EditText edtTel1;
    EditText edtTel2;
    EditText edtDireccion;
    EditText edtNotas;
    CheckBox cbkFavorito;
    Contacto saveContact;
    int savedIndex;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtNombre = (EditText) findViewById(R.id.txtNombre);
        edtTel1 = (EditText) findViewById(R.id.txtTel1);
        edtTel2 = (EditText) findViewById(R.id.txtTel2);
        edtDireccion = (EditText) findViewById(R.id.txtDomicilio);
        edtNotas = (EditText) findViewById(R.id.txtNota);
        cbkFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnSalir = (Button) findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /* boton save y update*/
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtNombre.getText().toString().equals("")
                || edtDireccion.getText().toString().equals("")
                || edtTel1.getText().toString().equals("")){
                    Toast.makeText(MainActivity.this, R.string.msgerror,Toast.LENGTH_SHORT).show();
                }else{
                    Contacto nContacto = new Contacto();
                    int index = contacto.size();
                    if(saveContact != null){
                        contacto.remove(savedIndex);
                        nContacto=saveContact;
                        index = savedIndex;
                    }
                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setTel1(edtTel1.getText().toString());
                    nContacto.setTel2(edtTel2.getText().toString());
                    nContacto.setFavorito(cbkFavorito.isChecked());
                    nContacto.setNotas(edtNotas.getText().toString());
                    savedIndex = 0;
                    contacto.add(savedIndex,nContacto);
                    //savedIndex++;
                    Toast.makeText(MainActivity.this,R.string.mensaje, Toast.LENGTH_SHORT).show();
                    limpiar();
                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this,ListActivity.class);
                Bundle bObjet = new Bundle();
                bObjet.putSerializable("contacto",contacto);
                i.putExtras(bObjet);
                startActivityForResult(i,0);
            }
        });
    }
    protected void onActivityResult(int requesCode, int resultCode,Intent intent){
        super.onActivityResult(requesCode,resultCode,intent);
        if(intent != null){
            Bundle oBundle = intent.getExtras();
            saveContact = (Contacto) oBundle.getSerializable("contacto");
            savedIndex = oBundle.getInt("index");
            edtNombre.setText(saveContact.getNombre());
            edtTel1.setText(saveContact.getTel1());
            edtTel2.setText(saveContact.getTel2());
            edtDireccion.setText(saveContact.getDomicilio());
            edtNotas.setText(saveContact.getNotas());
            cbkFavorito.setChecked(saveContact.isFavorito());
        }else{
            limpiar();
        }
    }
    private boolean validarCampos(){
        return true;
    }
    private void limpiar(){
        edtNombre.setText("");
        edtDireccion.setText("");
        edtTel1.setText("");
        edtTel2.setText("");
        edtNotas.setText("");
        cbkFavorito.setChecked(false);
    }
}
